var User = require('../models/user');
var UserValidator = require('../validations/uservalidator');

var response = {};

class Users {

    index(req, res) {

        User.all((data) => {
            res.status(data.status).json({
                message: data.message,
                data: data.data || {}
            });
        });
    }

    show(req, res) {
        let id = req.params.id;
        User.get(id, (data) => {
            res.status(data.status).json({
                message: data.message,
                data: data.data || {}
            });
        });
    }

    store(req, res) {

        UserValidator.validateSignup(req).then(() => {
            let result = User.save(req, (data) => {
                res.status(data.status).json({
                    message: data.message,
                    data: data.data || {}
                });
            });
        }, (error) => {
            res.status(400).json({
                error: error
            });
        });
    }

    destroy(req, res) {
        let id = req.params.id;
        User.delete(id, (data) => {

            res.status(data.status).json({
                message: data.message,
                data: data.data || {}
            });
        });
    }

    login(req, res) {

        UserValidator.validateLogin(req).then(() => {
            User.login(req, (data) => {
                res.status(data.status).json({
                    message: data.message,
                    data: data.data || {}
                });
            });
        }, (error) => {
            res.status(400).json({
                error: error
            });
        });
    }

    status(req, res) {
        let id = req.params.id;
        User.status(id, (data) => {
            res.status(data.status).json({
                message: data.message,
                data: data.data || {}
            });
        });
    }

    update(req, res) {
        let id = req.params.id;
        User.update(id, req, (data) => {
            res.status(data.status).json({
                message: data.message,
                data: data.data || {}
            });
        });
    }

};
module.exports = new Users();