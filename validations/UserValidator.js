var util = require('util');
class UserValidator {

    validateLogin(req) {

        return new Promise((resolve, reject) => {

            req.checkBody('email', 'Email is required').notEmpty();
            req.checkBody('email', 'Please Provide Valid Email').isEmail();
            req.checkBody('password', 'Password is required').notEmpty();

            req.getValidationResult().then(function (result) {
                if (!result.isEmpty()) {
                    reject(result.array());
                } else {
                    resolve();
                }
            });
        });
    }

    validateSignup(req) {
        return new Promise((resolve, reject) => {
            req.checkBody('firstname', 'First Name is required').notEmpty();
            req.checkBody('firstname', 'Please enter valid First Name').isAlpha();
            req.checkBody('lastname', 'Last Name is required').notEmpty();
            req.checkBody('lastname', 'Please enter valid Last Name').isAlpha();
            req.checkBody('email', 'Email is required').notEmpty();
            req.checkBody('email', 'Please Provide Valid Email').isEmail();
            req.checkBody('password', 'Password is required').notEmpty();

            req.getValidationResult().then(function (result) {
                if (!result.isEmpty()) {
                    reject(result.array());
                } else {
                    resolve();
                }
            });
        });
    }


}

module.exports = new UserValidator();